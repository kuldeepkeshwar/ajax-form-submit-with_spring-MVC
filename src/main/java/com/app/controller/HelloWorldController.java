package com.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController { 
    @RequestMapping(value="/addPerson", method=RequestMethod.POST)
    public ResponseEntity<Object> checkPersonInfo(@Valid Person person, BindingResult bindingResult) {
    	HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ValidationUtils.rejectIfEmpty(bindingResult, "name", "Name can't be empty.");
        
    	System.out.println("addPerson: "+person);
        if (bindingResult.hasErrors()) {
        	System.out.println("addPerson:bad request");
        	return new ResponseEntity<Object>(bindingResult.getAllErrors(),headers, HttpStatus.BAD_REQUEST);
        }
        System.out.println("addPerson:status :ok");
        return new ResponseEntity<Object>(person, HttpStatus.OK);
    }
}
