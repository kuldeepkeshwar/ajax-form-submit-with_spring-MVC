<html>
  <head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link href="resources/dist/styles/vendor.css"  rel="stylesheet">
	
	<script src="resources/dist/scripts/vendor.js"></script>
  	<script src="resources/dist/scripts/scripts.js"></script>
	<script src="resources/js/script.js"></script>	
  </head>
 
  <body ng-app='myApp' class="container">
	<div class="well" ng-controller="MyCtrl">
		<h3 class="text-center">Submit Form Using Ajax</h3>
        <form class="form-horizontal" ajax-submit="ajaxOptions" action="addPerson" method="POST" name="myform">
			<div class="form-group">
				<ajax-submit-response></ajax-submit-response>
            </div>
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="name" name="name" ng-model="name" placeholder="Enter name">
				</div>				
			</div>
			<div class="form-group">
				<label for="age" class="col-sm-2 control-label">Age</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="age" name="age" ng-model="age" placeholder="Enter age">
				</div>				
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				  <button type="submit" class="btn btn-success">Submit</button>
				</div>
			</div>			  
        </form>
		<p>Name can't be empty</p>
		<p>Age should be numeric</p>
    </div> 
  </body>
</html>

