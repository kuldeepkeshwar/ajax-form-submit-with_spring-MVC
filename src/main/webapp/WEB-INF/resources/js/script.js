		// Code goes here
var myApp = angular.module('myApp', ['ajaxFormSubmitApp']);
myApp.controller('MyCtrl', ['$scope','$log','$q',function($scope,$log,$q) {
    $scope.name = 'Boop';
    $scope.age = 15;
    $scope.$watch('age',function(n){
        if(n==100){
          $scope.myform.age.$setValidity('UiValidationFailed', false);
        }else{
          $scope.myform.age.$setValidity('UiValidationFailed', true);
        }
    })
  	$scope.ajaxOptions={
  		pre:function(){
        var deferred = $q.defer();
        deferred.resolve();
        $log.log('pre:hook');
        return deferred.promise;
  		},
  		success:function(resp){
  			$log.log('success:hook',resp);
  		},
  		error:function(resp){
  			$log.log('error:hook',resp);
  		}
  	}
}]);
