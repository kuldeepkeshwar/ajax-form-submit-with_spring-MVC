'use strict';

/**
 * @ngdoc overview
 * @name ajaxFormSubmitApp
 * @description
 * # ajaxFormSubmitApp
 *
 * Main module of the application.
 */
angular
  .module('ajaxFormSubmitApp', []);

'use strict';

/**
 * @ngdoc function
 * @name ajaxFormSubmitApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ajaxFormSubmitApp
 */
angular.module('ajaxFormSubmitApp')
  .controller('MainCtrl', function ($scope) {
    
  });

'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmit
 * @description
 * # ajaxSubmit
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmit', ['$compile', '$http','$q',function($compile, $http,$q) {
    var _defaultPreFn=function(){
      var deferred = $q.defer();
      deferred.resolve();
      return deferred.promise;
    }
	return {
      restrict: 'A',
      require: ['form','ajaxSubmit'],
      scope: {
  		  options:'=ajaxSubmit'
  	  },
      controller:['$scope',function($scope){
        var errors=[];
        var success=false;
        this.getError=function(){
          return errors;
        }
        this.setError=function(data){
          errors=data;
        }
        this.getSuccess=function(){
          return success;
        }
        this.setSuccess=function(data){
          success=data;
        }   
      }],
      link:function(scope, element, attr, controller) {
        var _options,_processError,_processSuccess,_cleanUp,_submit,_resp,formCtrl,ajaxSubmitCtrl;
        formCtrl=controller[0];
        ajaxSubmitCtrl=controller[1];
  			var _processError=function(data){
          ajaxSubmitCtrl.setError(data);
        };
        var _processSuccess=function(data){
          ajaxSubmitCtrl.setSuccess(data);
        };
  			var _cleanUp=function(){
  				angular.forEach(formCtrl, function(e,pro) {
            if(pro.indexOf('$')==-1 && angular.isFunction(e.$setValidity))
               e.$setValidity('ServerValidationFailed', true);
          });
          ajaxSubmitCtrl.setError([]);
          ajaxSubmitCtrl.setSuccess(false);
        };
  			var _submit = function(e) {
          if (e.preventDefault)
            e.preventDefault();
          _cleanUp();
          
          if(!formCtrl.$valid) return;
          _options.pre().then(function(){
            _$submit();
          });
          return false;
        };
  			var _$submit=function(){
          var _request={
              method: attr.method,
              url: attr.action,
              data: $(element).serialize(),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          };
          
          $http(_request).success(function(data, status, headers, config) {
            _resp={
              status:status,
              data:data,
              headers:headers,
              config:config
            };
            _options.success(_resp);
            _processSuccess(true);
          }).error(function(data, status, headers, config) {
            _resp={
              status:status,
              data:data,
              headers:headers,
              config:config
            };
            _options.error(_resp);
            _processError(_resp.data);
          });
        };
  			
        var _options={
          pre:_defaultPreFn,
          success:angular.noop,
          error:angular.noop,
        };
        angular.extend(_options,scope.options);
        element.on("submit", _submit);        
    }
  };
}]);

'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitSuccess
 * @description
 * # ajaxSubmitSuccess
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmitSuccess', function () {
    var templateFn=function(){
      return '<div ng-show="submitted" role="alert" class="alert alert-success"><button ng-click="hide()" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>'+
              '<p>{{successMsg}}</p>'+
            '</div>';
    }
    return {
      template:templateFn,
      require: ['^ajaxSubmit'],
      restrict: 'E',scope:{},
      link: function postLink(scope, element, attrs,controllers) {
          var ajaxSubmitCtrl;
          ajaxSubmitCtrl=controllers[0];
          scope.submitted=false;
          scope.successMsg = "Form submitted !!";
          scope.hideSuccess=function(){
            scope.submitted=false;
          }
          var _processSuccess=function(data){
            if(data===true){
              scope.submitted=true;
            }else{
              scope.submitted=false;
            }
          };
          scope.$watch(function(){
            return ajaxSubmitCtrl.getSuccess();
          },function(n){
              _processSuccess(n);
          });
          scope.hide=function(){
            ajaxSubmitCtrl.setSuccess(false);
          }
        }
      };
});
'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitError
 * @description
 * # ajaxSubmitError
 */
angular.module('ajaxFormSubmitApp').directive('ajaxSubmitError',  function () {
    var templateFn=function(){
      return '<div ng-show="errors.length!=0;" role="alert" class="alert alert-danger"><button ng-click="hide()" class="close" type="button"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>'+
              '<p ng-repeat="e in errors"><a href="#{{e.field}}">{{e.code}}</a></p>'+
            '</div>';
    }
    return {
      template:templateFn,
      require: ['^ajaxSubmit','^form'],
      restrict: 'E',scope:{},
      link: function postLink(scope, element, attrs,controllers) {
          var ajaxSubmitCtrl,formCtrl;
          ajaxSubmitCtrl=controllers[0];
          formCtrl=controllers[1];
          scope.errors = [];
          var _removeError=function(){
              angular.forEach(scope.errors, function(e, index) {
                if(formCtrl[e.field])
                   formCtrl[e.field].$setValidity('ServerValidationFailed', true);
              });
              scope.errors = [];
          }
          var _processError=function(data){
            if(angular.isArray(data)){
              scope.errors = data;
              angular.forEach(scope.errors, function(e, index) {
                if(formCtrl[e.field])
                   formCtrl[e.field].$setValidity('ServerValidationFailed', false);
              });
            }else{
              _removeError();
            }            
          };
          scope.$watch(function(){
            return ajaxSubmitCtrl.getError();
          },function(n){
              _processError(n);
          });
          scope.hide=function(){
            ajaxSubmitCtrl.setError([]);
          }
        }
      };
});

'use strict';

/**
 * @ngdoc directive
 * @name ajaxFormSubmitApp.directive:ajaxSubmitResponse
 * @description
 * # ajaxSubmitResponse
 */
angular.module('ajaxFormSubmitApp')
  .directive('ajaxSubmitResponse', function () {
    return {
      template:"<ajax-submit-error></ajax-submit-error><ajax-submit-success></ajax-submit-success>",
      require: ['^ajaxSubmit']
    };
  });
